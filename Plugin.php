<?php namespace StudioBosco\ActivityPub;

use Backend;
use Lang;
use System\Classes\PluginBase;

/**
 * ActivityPub Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => Lang::get('studiobosco.activitypub::plugin.name'),
            'description' => Lang::get('studiobosco.activitypub::plugin.description'),
            'author'      => 'Ondrej Brinkel <ondrej@studiobosco.de>',
            'icon'        => 'icon-camera',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'StudioBosco\ActivityPub\Components\Posts' => 'activityPubPosts',
        ];
    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [];
    }

    public function registerSettings()
    {
        return [];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];
    }
}
