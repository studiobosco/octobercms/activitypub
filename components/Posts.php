<?php namespace StudioBosco\ActivityPub\Components;


use Lang;
use Input;
use Cms\Classes\ComponentBase;
use StudioBosco\ActivityPub\Classes\ActivityPubApi;

class Posts extends ComponentBase
{
    public $posts = [];
    public $pagination = false;
    public $firstId = null;
    public $lastId = null;

    public function componentDetails()
    {
        return [
            'name'        => Lang::get('studiobosco.activitypub::posts.name'),
            'description' => Lang::get('studiobosco.activitypub::posts.description'),
        ];
    }

    public function defineProperties()
    {
        return [
            'url' => [
                'title' => Lang::get('studiobosco.activitypub::posts.url.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.url.description'),
            ],
            'access_token' => [
                'title' => Lang::get('studiobosco.activitypub::posts.access_token.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.access_token.description'),
            ],
            'user' => [
                'title' => Lang::get('studiobosco.activitypub::posts.user.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.user.description'),
            ],
            'limit' => [
                'title' => Lang::get('studiobosco.activitypub::posts.limit.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.limit.description'),
                'default' => 4,
            ],
            'visibility' => [
                'title' => Lang::get('studiobosco.activitypub::posts.visibility.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.visibility.description'),
                'type' => 'dropdown',
                'options' => [
                    '' => Lang::get('studiobosco.activitypub::posts.visibility.options.any'),
                    'public' => Lang::get('studiobosco.activitypub::posts.visibility.options.public'),
                    'unlisted' => Lang::get('studiobosco.activitypub::posts.visibility.options.unlisted'),
                    'private' => Lang::get('studiobosco.activitypub::posts.visibility.options.private'),
                    'direct' => Lang::get('studiobosco.activitypub::posts.visibility.options.direct'),
                ],
                'default' => '',
            ],
            'withMediaOnly' => [
                'title' => Lang::get('studiobosco.activitypub::posts.withMediaOnly.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.withMediaOnly.description'),
                'type' => 'checkbox',
                'default' => false,
            ],
            'pagination' => [
                'title' => Lang::get('studiobosco.activitypub::posts.pagination.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.pagination.description'),
                'type' => 'checkbox',
                'default' => false,
            ],
            'activityPubType' => [
                'title' => Lang::get('studiobosco.activitypub::posts.activityPubType.title'),
                'description' => Lang::get('studiobosco.activitypub::posts.activityPubType.description'),
                'type' => 'dropdown',
                'options' => $this->getActivityPubTypeOptions(),
            ],
        ];
    }

    protected function prepareVars()
    {
        $url = $this->property('url');
        $accessToken = $this->property('access_token');
        $userId = $this->property('user');
        $limit = intval($this->property('limit', 10));
        $visibility = $this->property('visibility', '');
        $activityPubType = $this->property('activityPubType', null);
        $withMediaOnly = boolval(intval($this->property('withMediaOnly', 0)));
        $this->pagination = boolval(intval($this->property('pagination', 0)));
        $minId = Input::get('min_id', null);
        $maxId = Input::get('max_id', null);

        if (!$url) {
            throw new \Exception('Missing URL.');
        }

        if (!$accessToken) {
            throw new \Exception('Missing access token.');
        }

        if (!$userId) {
            throw new \Exception('Missing user ID.');
        }

        $api = new ActivityPubApi($url, $accessToken, ['activityPubType' => $activityPubType]);
        
        $query = [
            'limit' => $limit,
        ];
        if ($withMediaOnly) {
            $query['only_media'] = 1;
        }
        if (!$maxId && !$minId) {
            $minId = 1;
        }
        if ($minId) {
            $query['min_id'] = $minId;
        }
        if ($maxId) {
            $query['max_id'] = $maxId;
        }

        $this->posts = array_filter($api->accountStatusesById($userId, $query) ?? [], function ($post) use ($visibility, $withMediaOnly) {
            $include = true;

            if ($visibility) {
                $include = $post['visibility'] === $visibility;
            }

            if ($include && $withMediaOnly) {
                $include = isset($post['media_attachments']) && count($post['media_attachments']);
            }

            return $include;
        });
        
        if ($limit) {
            $this->posts = array_slice($this->posts, 0, $limit);
        }
        
        if (count($this->posts)) {
            $this->firstId = array_first($this->posts)['id'];
            $this->lastId = last($this->posts)['id'];
        }
    }

    public function onRun()
    {
        $this->prepareVars();
    }

    public function onNextPage()
    {
        $this->prepareVars();
        $postsKey = '@#' . $this->alias . '-posts';
        $paginationKey = '#' . $this->alias . '-pagination';

        return [
            $postsKey => $this->renderPartial('@posts.htm'),
            $paginationKey => $this->renderPartial('@pagination.htm'),
        ];
    }

    protected function getActivityPubTypeOptions()
    {
        return [
            '' => 'generic',
            'pixelfed' => 'pixelfed',
        ];
    }
}
