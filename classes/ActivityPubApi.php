<?php namespace StudioBosco\ActivityPub\Classes;

use \Zttp\Zttp;
use \Exception;

class ActivityPubApi
{
    /** @var string */
    protected $domain;

    /** @var string */
    protected $activityPubType;

    /** @var \Zttp\Zttp */
    protected $client;

    /** @var string */
    protected $curl;

    /** @var array */
    protected $params = [];

    /** @param string $domain */
    /** @param string $accessToken|null */
    public function __construct(string $domain, $accessToken = null, array $options = [])
    {
        $this->domain = $domain;
        $this->activityPubType = array_get($options, 'activityPubType', null);
        $this->client = !$accessToken ? Zttp::new() : Zttp::new()->withHeaders([
            'Authorization' => "Bearer {$accessToken}"
        ]);
    }

    protected function furl(string $url)
    {
        $this->curl = $this->domain . $url;
    }

    public function user()
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/verify_credentials");
                break;
            }
            default: $this->furl("/api/v1/accounts/verify_credentials");
        }
        
        return $this->get();
    }

    public function accountById(string $id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}");
        }
        
        return $this->get();
    }

    public function accountFollowersById(string $id, array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/followers" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/followers" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function accountFollowingById(string $id, array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/following" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/following" . $this->queryToString($query));
        }
        
        return $this->get();
    }

    public function accountStatusesById(string $id, array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/statuses" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/statuses" . $this->queryToString($query));
        }
        
        return $this->get();
    }

    public function nodeinfo()
    {
        $this->furl("/api/nodeinfo/2.0.json");
        return $this->get();
    }

    public function accountSearch($search, array $query = [])
    {
        $query['q'] = $search;

        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/search" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/accounts/search" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function accountBlocks()
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/blocks");
                break;
            }
            default: $this->furl("/api/v1/blocks");
        }

        return $this->get();
    }

    public function accountLikes(array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/favourites" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/favourites" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function accountFollowRequests(array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/follow_requests" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/follow_requests" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function instance()
    {
        $this->furl("/api/v1/instance");
        return $this->get();
    }

    public function accountMutes(array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/mutes" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/mutes" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function accountNotifications(array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/notifications" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/notifications" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function homeTimeline(array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/timelines/home" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/timelines/home" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function publicTimeline(array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/timelines/public" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/timelines/public" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function statusById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/statuses/{$id}");
                break;
            }
            default: $this->furl("/api/v1/statuses/{$id}");
        }

        return $this->get();
    }

    public function statusRebloggedById($id, array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/statuses/{$id}/reblogged_by" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/statuses/{$id}/reblogged_by" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function statusLikedById($id, array $query = [])
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/statuses/{$id}/favourited_by" . $this->queryToString($query));
                break;
            }
            default: $this->furl("/api/v1/statuses/{$id}/favourited_by" . $this->queryToString($query));
        }

        return $this->get();
    }

    public function followAccountById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/follow");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/follow");
        }

        return $this->post();
    }

    public function unfollowAccountById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/unfollow");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/unfollow");
        }

        return $this->post();
    }

    public function accountBlockById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/block");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/block");
        }

        return $this->post();
    }

    public function accountUnblockById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/unblock");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/unblock");
        }

        return $this->post();
    }

    public function statusFavouriteById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/statuses/{$id}/favourite");
                break;
            }
            default: $this->furl("/api/v1/statuses/{$id}/favourite");
        }

        return $this->post();
    }

    public function statusUnfavouriteById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/statuses/{$id}/unfavourite");
                break;
            }
            default: $this->furl("/api/v1/statuses/{$id}/unfavourite");
        }

        return $this->post();
    }

    public function mediaUpload($file)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/media");
                break;
            }
            default: $this->furl("/api/v1/media");
        }

        $this->params = [[
            'name' => 'file',
            'contents' => $file,
            'filename' => 'tmp.jpg'
        ]];
        return $this->multipartPost();
    }

    public function accountMuteById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/mute");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/mute");
        }

        return $this->post();
    }

    public function accountUnmuteById($id)
    {
        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/accounts/{$id}/unmute");
                break;
            }
            default: $this->furl("/api/v1/accounts/{$id}/unmute");
        }

        return $this->post();
    }

    public function statusCreate($mediaIds = [], $caption = null, bool $sensitive = false, $scope = 'public', $inReplyToId = null)
    {
        if(empty($mediaIds) || !is_array($mediaIds)) {
            throw new Exception('Invalid media_ids. Must be an array of integers.');
        }

        if(!in_array($scope, ['private','unlisted','public'])) {
            throw new Exception('Invalid scope. Must be private, unlisted or public');
        }

        switch($this->activityPubType) {
            case 'pixelfed': {
                $this->furl("/api/pixelfed/v1/statuses");
                break;
            }
            default: $this->furl("/api/v1/statuses");
        }

        $this->params = [
            'media_ids' => $mediaIds,
            'status' => $caption,
            'in_reply_to_id' => $inReplyToId,
            'sensitive' => $sensitive,
            'visibility' => $scope
        ];
        return $this->post();
    }

    protected function get()
    {
        return $this->client->get($this->curl)->json();
    }

    protected function multipartPost()
    {
        return $this->client->asMultipart()->post($this->curl, $this->params)->json();
    }

    protected function post()
    {
        return $this->client->post($this->curl, $this->params)->json();
    }

    protected function queryToString(array $query = [])
    {
        $str = '';

        if (count($query)) {
            $str = '?' . http_build_query($query);
        }

        return $str;
    }
}
