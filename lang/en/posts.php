<?php return [
    'name' => 'ActivityPub Posts',
    'description' => 'Displays latest posts',
    'user' => [
        'title' => 'User ID',
        'description' => 'ID of the user to show posts for.',
    ],
    'limit' => [
        'title' => 'Limit',
        'description' => 'Limit number of posts to this.',
    ],
    'visibility' => [
        'title' => 'Visibility',
        'description' => 'Only show posts with this visibility.',
        'options' => [
            'any' => 'any',
            'public' => 'public',
            'unlisted' => 'unlisted',
            'private' => 'private',
            'direct' => 'direct',
        ],
    ],
    'withMediaOnly' => [
        'title' => 'only with media',
        'description' => 'Only include posts with media',
    ],
    'url' => [
        'title' => 'URL',
        'description' => 'URL to an activity-pub server',
    ],
    'access_token' => [
        'title' => 'Access Token',
        'description' => 'Your personal access token',
    ],
    'pagination' => [
        'title' => 'Pagination',
        'description' => 'If checked a pagination will be used.',
    ],
    'activityPubType' => [
        'title' => 'Activity pub type',
        'description' => 'Type of the activity pub application.',
    ],
];
