<?php return [
    'name' => 'ActivityPub',
    'namespace' => 'ActivityPub',
    'description' => 'Integrates ActivityPub into OctoberCMS',
];
